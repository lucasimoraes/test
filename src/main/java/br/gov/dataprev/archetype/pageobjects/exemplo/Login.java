package br.gov.dataprev.archetype.pageobjects.exemplo;

import br.gov.dataprev.dta2.base.LocationDescriptor;
import br.gov.dataprev.dta2.pageobjects.GenericPageObject;

public class Login extends GenericPageObject {
	//acessar sistema
	public void accessSystem(){
		getEngine().clicarPorLocator(new LocationDescriptor("//ul[2]/li/a"));
	}
	//fazer login no sistema
	public void login(String email, String password){
		getEngine().escreverPorId("user_email", email);
		getEngine().escreverPorId("user_password", password);
	}
	//clicar no bot�o entrar
	public void btnSingIn(){
		getEngine().clicarPorValue("Sign in");
	}
	
	//verificar se login efetuado com sucesso
	public String checkAccess(){
		return getEngine().obterTextoPorLocator(new LocationDescriptor("//body/div/div[2]"));
	}
	
	
}
