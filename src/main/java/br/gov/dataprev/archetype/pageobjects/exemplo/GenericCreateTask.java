package br.gov.dataprev.archetype.pageobjects.exemplo;

import br.gov.dataprev.dta2.base.LocationDescriptor;
import br.gov.dataprev.dta2.pageobjects.GenericPageObject;

public class GenericCreateTask extends GenericPageObject {
	
	//clicar link My Tasks
	public void acessarMyTask(){
		getEngine().clicarPorLocator(new LocationDescriptor("//li[2]/a"));
	}
	
	//armazenar usuario logado
	public String userLogado(){
		return getEngine().obterTextoPorLocator(new LocationDescriptor("//ul[2]/li/a"));		
	}
	
	//verificar texto da mensagem para usu�rio logado
	public String msgUsuario(){
		return getEngine().obterTextoPorLocator(new LocationDescriptor("//h1")).trim();
	}
	
	//preencher campo new_task
	
	public void newTask(String newtask){
		getEngine().escreverPorId("new_task", newtask);
	}
	
	public void waitTime(int time){
		getEngine().esperar(time);
	}
	
	//clicar no bot�o add
	public void btnadd(){
		getEngine().clicarPorLocator(new LocationDescriptor("//div[2]/span"));
	}
	//verificar task criada
	public String verificarTask(){
		return getEngine().obterTextoPorLocator(new LocationDescriptor("//td[2]"));
	}
	
	//clicar em Manage SubTasks
	public void btnManageSubTask(){
		getEngine().clicarPorLocator(new LocationDescriptor("//td[4]/button"));
		
	}
	
	// informar campo new_sub_task
	public void newSubTask(String valor){
		getEngine().escreverPorId("new_sub_task", valor);
	}
	
	//informar campo Due Date
	public void DueDate(String date){
		getEngine().escreverPorId("dueDate", date);
	}
	
	//bot�o add subtask
	public void btnAddSubTask(){
		getEngine().clicarPorID("add-subtask");
	}
	
	
	//verificar subTask add
	public String verificarSubTaskAdd(){
		return getEngine().obterTextoPorLocator(new LocationDescriptor("//div[2]/table/tbody/tr/td[2]"));
	}
	

}
