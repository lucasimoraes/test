package br.gov.dataprev.archetype.roteiros.exemplo;

import org.junit.Assert;
import org.junit.Test;

import br.gov.dataprev.archetype.pageobjects.exemplo.GenericCreateTask;
import br.gov.dataprev.archetype.pageobjects.exemplo.Login;
import br.gov.dataprev.dta2.base.BaseRoteiro;

public class CreateSubTask extends BaseRoteiro {
	
	@Test
	public void test(){
		Login lg = new Login();		
		
		lg.accessSystem();
				
		lg.login("lucasimoraes@yahoo.com.br", "A123456b");
		
		lg.btnSingIn();	
		//verificar login com sucesso
		Assert.assertEquals("Signed in successfully.", lg.checkAccess());
		
		GenericCreateTask page = new GenericCreateTask();
		//Acessar op��o My Taks
		page.acessarMyTask();
		//Clicar na op��o Manage SubTask
		page.btnManageSubTask();
		
		String SubTaskText = "Nunc et elit sodales, interdum dolor vel, convallis ante. Etiam in enim porta, congue tellus et, eleifend nisl. Nulla facilisi. Nulla sed mauris risus. Vivamus ut dolor aliquet augue molestie semper non eget erat. Nulla ac pulvinar turpis. Aliquam sfvf";
		String DueDate = "01/03/2016";				
		//Preencher campo SubTask
		page.newSubTask(SubTaskText);
		
		//int tam = SubTaskText.length();
		//Prencher campo Due Date
		page.DueDate(DueDate);
		
		page.btnAddSubTask();
				
		Assert.assertEquals(SubTaskText, page.verificarSubTaskAdd());
		
	}

}
