package br.gov.dataprev.archetype.roteiros.exemplo;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.junit.Assert;

import br.gov.dataprev.archetype.pageobjects.exemplo.GenericCreateTask;
import br.gov.dataprev.archetype.pageobjects.exemplo.Login;
import br.gov.dataprev.dta2.base.BaseRoteiro;

public class CreateTask extends BaseRoteiro {
	private static WebDriver driver = null;
	@Test
	
	public void test(){
		Login lg = new Login();		
		
		lg.accessSystem();
				
		lg.login("lucasimoraes@yahoo.com.br", "A123456b");
		
		lg.btnSingIn();	
		//verificar login com sucesso
		Assert.assertEquals("Signed in successfully.", lg.checkAccess());
		
		GenericCreateTask page = new GenericCreateTask();
		//receber usu�rio logado
		String usuarioLogado1 = page.userLogado();
		
		String usuarioLogado2 = usuarioLogado1.substring(9, 30);		
		
		page.acessarMyTask();
		
		
		//verificar mensagem usu�rio logado
		
		//Assert.assertEquals("Lucas Iglesias Moraes"+"'s ToDo List", page.msgUsuario());
		Assert.assertEquals(usuarioLogado2+"'s ToDo List", page.msgUsuario());
		
		String task1 = "Ta";
		String task2 = "Nunc et elit sodales, interdum dolor vel, convallis ante. Etiam in enim porta, congue tellus et, eleifend nisl. Nulla facilisi. Nulla sed mauris risus. Vivamus ut dolor aliquet augue molestie semper non eget erat. Nulla ac pulvinar turpis. Aliquam sed el";
		//prencher campo New Task com menos de 3 caracteres
		page.newTask(task1);
		page.btnadd();
		Assert.assertEquals(task1, page.verificarTask());
		//prencher campo New Task com mais de 250 caracteres
		page.newTask(task2);
		page.btnadd();
		Assert.assertEquals(task2, page.verificarTask());	
		
		page.waitTime(2);
		//driver.findElement(By.id("Value")).sendKeys(Keys.ENTER);
		
				
		
	}
	
	
}
